<?php
get_header();
while (have_posts()):
the_post();
?>
<main id="main" class="main">
    <div class="room-detail-sec sec sec-70">
        <div class="container">
            <div class="main-title-wrap">
                <h2 class="main-sub-title"><?php echo __('Restaurant & Bar','monamedia') ?></h2>
                <h1 class="main-title hl-color"><?php the_title() ?></h1>
            </div>
            <div class="room-detail-img full-img">
                <?php echo wp_get_attachment_image( get_field('image_banner_single') , '1050x999' ) ?>
            </div>
            <div class="room-detail-info text-wrap">
                <div class="columns">
                    <div class="column">
                        <div class="hr-thin mt-0"></div>
                        <a target="_blank" href="<?php echo get_field('rest_file_pdf')  ?>" class="readmore-btn"><?php echo get_field('rest_view_text') ?> </a>
                    </div>
                    <div class="column">
                        <p class="room-detail-info-title"><?php echo get_field('rest_title_info') ?> </p>
                        <div class="room-detail-info-text">
                            <p><?php echo get_field('rest_info') ?> </p>
                        </div>
                    </div>
                    <div class="column">
                        <p class="room-detail-info-title"><?php echo get_field('rest_title_time') ?> </p>
                        <div class="room-detail-info-text">
                            <p> <?php echo get_field('rest_time') ?> </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="room-detail-gallery">
    <?php
    $rest_ga = get_field('rest_gal');
    if (is_array($rest_ga)) {
        foreach ($rest_ga as $item) { ?>

            <div class="room-detail-gallery-img">
                <img src="<?php echo wp_get_attachment_image_url($item['rest_img'], 'full') ?>" alt="">
                </div>
<?php } } ?>
            </div>
        </div>
    </div>
    <?php
    $restChoose = get_field('rest_others');
    $args = array(
        'post_type'         => 'mona_restaurant',
        'posts_per_page'    => 5,
        'post__not_in'      => array(get_the_ID()),
        // 'post__in'          => $postChoose ,
    );
    if(is_array($restChoose) && count($restChoose) > 0){
        $args['post__in'] = $restChoose;
        $args['orderby'] = 'post__in';
    }
    $orderQuery = new WP_Query($args);
    if($orderQuery->have_posts()){
        ?>
    <section class="order-rooms-sec white-bg sec sec-60 sec-35-respon">
        <div class="container">
            <div class="main-title-wrap text-center">
                <h2 class="main-title md-title">Other rooms</h2>
            </div>
            <div class="order-rooms-wrap">
                <div class="columns columns-4 columns-center">
                    <?php
                    while($orderQuery->have_posts()){
                        $orderQuery->the_post();
                        ?>
                        <div class="column">
                        <div class="offers-item">
                            <div class="offers-img">
                                <div class="offers-img-top">
                                    <a href="<?php the_permalink(  ) ?>">
                                        <?php the_post_thumbnail( '186x186' ) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="offers-info">
                                <h3 class="offers-info-title"><a href="<?php echo get_permalink(  ) ?>"><?php the_title() ?></a></h3>
                                <div class="hr-thin"></div>
                                <a href="<?php echo get_permalink(  ) ?>" class="readmore-btn"><?php _e('Read more','monamedia') ?></a>
                            </div>
                        </div>
                    </div>
                    <?php } wp_reset_query(  ); ?>
                </div>
            </div>
        </div>
    </section>
        <?php } ?>
</main>
<?php
endwhile;
get_footer();
?>
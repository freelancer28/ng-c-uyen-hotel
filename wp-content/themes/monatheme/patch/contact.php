<div class="columns ft-columns">
    <div class="column ft-column">
        <h3 class="ft-title lg-title lg-title-respon"><?php echo get_field('contact_ft',MONA_HOME) ?></h3>
        <ul class="ft-info">
            <?php
            $inft = get_field('in_ft',MONA_HOME);
            if (is_array($inft)) {
                foreach ($inft as $item) { ?>

                    <li>
                        <p class="ft-info-title"><?php echo $item['title_it'] ?></p>
                        <div class="ft-info-desc"><a href="<?php echo $item ['text_in']?>"><?php echo $item['in_it'] ?></a></div>
                    </li>
                <?php } } ?>
        </ul>
    </div>
    <div class="column ft-column">
        <?php
        $short = get_field('form_ft',MONA_HOME);
        if ($short != '') {
            echo do_shortcode($short);
        }
        ?>
    </div>
    <div class="column ft-column">
        <div class="ft-map"><?php echo get_field('map_ft',MONA_HOME) ?></div>
    </div>
</div>
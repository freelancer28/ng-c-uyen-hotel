<ul class="sns-ul clear">
    <?php $fb = mona_get_option('mona_sticky_header_facebook',''); ?>
    <?php echo($fb !=''? '<li><a href="'.$fb.'" target="_blank" class="square-30fa" ><i class="fa fa-facebook"></i></a></li>':'');?>
    
    <?php $ins = mona_get_option('mona_sticky_header_instagram',''); ?>
    <?php echo($ins  !=''? '<li><a href="'.$ins .'" target="_blank" class="square-30fa" ><i class="fa fa-instagram"></i></a></li>':'');?>
     
    <?php $youtube =  mona_get_option('mona_sticky_header_youtube','')?>
    <?php echo( $youtube !=''? '<li><a href="'.$youtube.'" target="_blank" class="square-30fa" ><i class="fa fa-youtube-play"></i></a></li>':'');?>

    <?php $twitter = mona_get_option('mona_sticky_header_twitter'); ?>
    <?php echo ( $twitter != '' ? '<li><a href="'.$twitter.'" target="_blank" class="square-30fa" ><i class="fa fa-twitter" aria-hidden="true"></i></a></li>' : '' ); ?>

    <?php $link = mona_get_option('mona_sticky_header_linkedin',''); ?>
    <?php echo($link !=''? '<li><a href="'.$link.'" target="_blank" class="square-30fa" ><i class="fa fa-linkedin-square"></i></a></li>':'');?>
</ul>

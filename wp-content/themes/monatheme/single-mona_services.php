<?php
get_header();
while (have_posts()):
    the_post();
    ?>
       <main id="main" class="main">
        <div class="services-detail-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h2 class="main-sub-title"><?php echo __('Facilities & Services','monamedia') ?></h2>
                    <h1 class="main-title hl-color"><?php the_title() ?></h1>
                    <div class="main-desc">
                        <?php echo get_field('info_service_single') ?>
                    </div>
                </div>
                <div class="services-detail-wrap">
                    <div class="mona-content">
                         <?php the_content(  ) ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>
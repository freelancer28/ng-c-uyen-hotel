<?php

//define('ACF_LITE', true);
define('MONA_HOME', 69);
define('MONA_BOOKING_NOW', 71);
define('MONA_ADMIN_MAIL', 'huuhung112999@gmail.com');
  
 
require_once( get_template_directory() . '/core/class/core.class.php' );
require_once( get_template_directory() . '/core/class/Mona_walker.php' );
require_once( get_template_directory() . '/core/class/hook.class.php' );
require_once( get_template_directory() . '/core/customizer.php' );
require_once( get_template_directory() . '/includes/functions.php' );
require_once( get_template_directory() . '/includes/ajax.php' );
 
// image size register
function mona_image_size() {

    add_image_size('1200x600', 1200, 600, true); // 2
    add_image_size('1050x360', 1050, 360, true);
    add_image_size('1010x380', 1010, 380, true);  //2,65
    add_image_size('690x460', 1200, 600, true); // 1.5
    add_image_size('510x382', 510, 382, true); //
    add_image_size('510x390', 510, 390, true); //
    add_image_size('500x500', 500, 500, true); // 1
    add_image_size('330x250', 330, 250, true); // 1.32
    add_image_size('330x185', 330, 185, true); // 1.78 
    add_image_size('242x182 ', 242, 182, true); // 1.32
    add_image_size('186x186', 186, 186, true); // 1 
    add_image_size('100x100', 100, 100, true); // 1

    add_image_size('1050x999', 1050, 999, false); 

}
add_action('after_setup_theme', 'mona_image_size');

function mona_register_menu() {
    register_nav_menus(
        [
            'primary-menu' => __('Theme Main Menu', 'monamedia'),
            'primary-menu-bottom' => __('Theme Main Menu Bottom', 'monamedia'),
            'primary-menu-user' => __('Theme Main Menu User', 'monamedia'),
            'footer-menu' => __('Theme Footer Menu', 'monamedia'),
            'top-menu' => __('Theme Top Menu', 'monamedia'),
            'video-menu' => __('Video Menu', 'monamedia'),
            'book-menu' => __('Book Menu', 'monamedia'),
        ]
    );
}

add_action('after_setup_theme', 'mona_register_menu');

function mona_register_sidebars() {
    register_sidebar(array(
        'id' => 'sidebar1',
        'name' => __('blog', 'mona_media'),
        'description' => __('The first (primary) sidebar.', 'mona_media'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="lbl">',
        'after_title' => '</h4>',
    ));

    // register widget item
    // require_once(get_template_directory() . '/widget/mona-cart.php');
    //  register_widget('mona_cart');
}

add_action('widgets_init', 'mona_register_sidebars');

function mona_style() {
    wp_enqueue_style('mona-custom', get_template_directory_uri() . '/css/mona-custom.css');
    wp_enqueue_script('mona-front', get_template_directory_uri() . '/js/front.js', array(), false, true);
    wp_enqueue_script('mona-notify', get_template_directory_uri() . '/js/notify.js', array(), false, true);
    wp_enqueue_script('mona-swelt', get_template_directory_uri() . '/js/sweetalert.min.js', array(), false, true);
    wp_localize_script('mona-front', 'mona_ajax_url', array('ajaxURL' => admin_url('admin-ajax.php'), 'siteURL' => get_site_url()));
}

add_action('wp_enqueue_scripts', 'mona_style');
 

function mona_add_custom_post() {
    $args = array(
        'labels' => array(
            'name' => 'Room',
            'singular_name' => 'Room',
            'add_new' => __('Add Room', 'monamedia'),
            'add_new_item' => __('New Room', 'monamedia'),
            'edit_item' => __('Edit Room', 'monamedia'),
            'new_item' => __('New Room', 'monamedia'),
            'view_item' => __('View Room', 'monamedia'),
            'view_items' => __('View Rooms', 'monamedia'),
        ),
        'description' => 'Add Room',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array('rooms'),
        'hierarchical' => false,
		'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'room',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-home',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('mona_room', $args);
    $tax_args = array(
        'labels' => array(
            'name' => __('Tasks', 'monamedia'),
            'singular_name' => __('Tasks', 'monamedia'),
            'search_items' => __('Search Tasks', 'monamedia'),
            'all_items' => __('All Tasks', 'monamedia'),
            'parent_item' => __('Parent Tasks', 'monamedia'),
            'parent_item_colon' => __('Parent Tasks', 'monamedia'),
            'edit_item' => __('Edit Tasks', 'monamedia'),
            'add_new' => __('Add Tasks', 'monamedia'),
            'update_item' => __('Update Tasks', 'monamedia'),
            'add_new_item' => __('Add New Tasks', 'monamedia'),
            'new_item_name' => __('New Tasks Name', 'monamedia'),
            'menu_name' => __('Tasks', 'monamedia'),
        ),
        'hierarchical' => true,
        'has_archive' => true,
        'public' => true,
        'rewrite' => array(
            'slug' => 'tasks',
            'with_front' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'publish_posts',
            'edit_terms' => 'publish_posts',
            'delete_terms' => 'publish_posts',
            'assign_terms' => 'publish_posts',
        ),
    );
    // register_taxonomy('tasks', 'task', $tax_args);
    $args = array(
        'labels' => array(
            'name' => 'Services',
            'singular_name' => 'Services',
            'add_new' => __('Add Services', 'monamedia'),
            'add_new_item' => __('New Services', 'monamedia'),
            'edit_item' => __('Edit Services', 'monamedia'),
            'new_item' => __('New Services', 'monamedia'),
            'view_item' => __('View Services', 'monamedia'),
            'view_items' => __('View Servicess', 'monamedia'),
        ),
        'description' => 'Add Services',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array(''),
        'hierarchical' => false,
		'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'services',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-media-text',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('mona_services', $args);
    $args = array(
        'labels' => array(
            'name' => 'Restaurant',
            'singular_name' => 'Restaurant',
            'add_new' => __('Add Restaurant', 'monamedia'),
            'add_new_item' => __('New Restaurant', 'monamedia'),
            'edit_item' => __('Edit Restaurant', 'monamedia'),
            'new_item' => __('New Restaurant', 'monamedia'),
            'view_item' => __('View Restaurant', 'monamedia'),
            'view_items' => __('View Restaurant', 'monamedia'),
        ),
        'description' => 'Add Restaurant',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array(''),
        'hierarchical' => false,
        'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'restaurant',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-store',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('mona_restaurant', $args);

    $args = array(
        'labels' => array(
            'name' => 'Tour',
            'singular_name' => 'Tour',
            'add_new' => __('Add Tour', 'monamedia'),
            'add_new_item' => __('New Tour', 'monamedia'),
            'edit_item' => __('Edit Tour', 'monamedia'),
            'new_item' => __('New Tour', 'monamedia'),
            'view_item' => __('View Tour', 'monamedia'),
            'view_items' => __('View Tour', 'monamedia'),
        ),
        'description' => 'Add Tour',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array(''),
        'hierarchical' => false,
        'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'tour',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-car',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('mona_tour', $args);
    
    flush_rewrite_rules();
}
add_action('init', 'mona_add_custom_post');



<?php 
/**
 * Template name: Offer Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/special-offers.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="s-offers-sec-wrap sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h1 class="main-title lg-title lg-title-respon hl-color"><?php the_title(  ) ?></h1>
                </div>
                <?php $offer  =get_field('item_offer_page');
                if( is_array ( $offer ) ) { 
                    foreach( $offer as $itemOffer ) {
                         ?>
                         
                <section class="s-offers-sec">
                    <div class="s-offers-img">
                        <div class="s-offers-img-top">
                            <a href="javascript:;">
                                <?php echo wp_get_attachment_image( $itemOffer['image'], '1050x360' ) ?>
                            </a>
                        </div> 
                    </div>
                    <div class="s-offers-info">
                        <h2 class="s-offers-title md-title"><?php echo $itemOffer['title']; ?></h2>
                        <div class="hr-thin"></div>
                        <a href="#popup-booking-offer" data-name="<?php echo $itemOffer['title']; ?>" class="readmore-btn open-popup-offer"><?php echo __('Book now','monamedia') ?></a>
                        <div class="s-offers-desc text-wrap">
                            <div class="columns">
                                 <?php $columns = $itemOffer['column'];
                                 if( is_array ( $columns ) ) { 
                                     foreach( $columns as $column ) {
                                          ?>
                                <div class="column">
                                    <p class="s-offers-desc-title"><?php echo $column['title'] ?></p>
                                    <div class="s-offers-desc-text">
                                       <?php echo $column['content'] ?>
                                    </div>
                                </div>         
                                          <?php
                                     }
                                 }
                                 ?> 
                            </div>
                        </div>
                    </div>
                </section>
                        <?php
                    }
                }
                ?> 
            </div>
        </div>
    </main>
    <div id="popup-booking-offer" class="mfp-hide main-popup">
        <h3 class="main-title text-center">Information</h3>
        <div class="ft-contact" id="booking-offer">
            <?php 
            $bookingShortCode = get_field('short_code_booking_offer');
            echo do_shortcode( $bookingShortCode ) ?>
        </div>
    </div>
    <?php
endwhile;
get_footer();
?>
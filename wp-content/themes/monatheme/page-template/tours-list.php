<?php 
/**
 * Template name: Tours List Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/tours.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="tours-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h1 class="main-title hl-color lg-title lg-title-respon"><?php the_title() ?></h1>
                </div>
                <?php 
                $count = 8;
                $page = max(1, get_query_var('paged'));
                $offset= ($page-1)*$count;
                
                $args = array( 
                    'post_type' => 'mona_tour',
                    'posts_per_page' => $count , 
                    'page'  => $page, 
                    'offset' => $offset,   
                );
                $tourQuery = new WP_Query($args);
                while($tourQuery->have_posts()) {
                    $tourQuery->the_post();
                    ?>
                <section class="tours-item">
                    <div class="tours-img full-img">
                        <a href="<?php the_permalink(  ) ?>">
                            <?php the_post_thumbnail( '1050x360' ) ?>
                        </a>
                    </div>
                    <div class="tours-info">
                        <h2 class="tours-info-title"><?php the_title() ?></h2>
                        <div class="hr-thin"></div>
                        <a href="<?php the_permalink(  ) ?>" class="readmore-btn"><?php echo __('Read more','monamedia') ?></a>
                    </div>
                    <div class="tours-desc">
                        <?php the_excerpt(  ) ?>
                    </div>
                </section>    
                    <?php
                }wp_reset_query(  );  
                ?> 
            </div> 
            <div class="container text-center mb-35">
                <?php mona_page_navi($tourQuery); ?>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>
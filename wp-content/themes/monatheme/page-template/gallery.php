<?php 
/**
 * Template name: Gallery Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/gallery.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="gallery-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h1 class="main-title lg-title lg-title-respon hl-color"><?php the_title() ?></h1>
                </div>
                <div class="gallery-wrap">
                    <div class="columns">
                        <?php $gallery = get_field('item_gallery_page');
                        if( is_array ( $gallery ) ) { 
                            foreach( $gallery as $item ) {
                                ?>
                        <div class="column">
                            <div class="gallery-item">
                                <div class="gallery-img">
                                    <?php echo wp_get_attachment_image( $item['image'] , '330x185' ) ?>
                                </div>
                                <h3 class="gallery-title"><?php echo $item['title'] ?></h3>
                                <div class="gallery-dys" data-thumb='[
                                <?php $id_gallery = $item['gallery'];
                                if( is_array ( $id_gallery ) ) { 
                                    foreach( $id_gallery as $key =>  $id ) {
                                        $img = wp_get_attachment_image_url($id ,'1200x600' );
                                        echo '{"src":"'.$img.'","thumb":"'.$img.'", "subHtml": "<h3>'.$item['title'].'</h3>"}';
                                        if( $key+1 < count($id_gallery) )
                                            echo ',';
                                    }
                                }
                                ?> 
                                ]'></div>
                            </div>
                        </div>
                                <?php 
                            }
                        }
                        ?> 
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>
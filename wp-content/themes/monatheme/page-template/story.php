<?php
/**
 * Template name: Story Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/gallery.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>

    <main id="main" class="main">
        <div class="services-detail-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h1 class="main-title hl-color"><?php the_title() ?></h1>
                </div>
                <div class="services-detail-wrap">
                    <div class="mona-content">
                        <div class="full-img mb-35">
                            <?php echo wp_get_attachment_image( get_field('image_banner_single') , '1050x999' ) ?>
                        </div>
<?php the_content() ?>

                        <div>
                            <p> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php
endwhile;
get_footer();
?>

<?php 
/**
 * Template name: Reservation 
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/reservation.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
      <main id="main" class="main">
        <div class="reservation-sec sec sec-70">
        <form action="" class="form-reservation">
            <div class="container">
                <div class="main-title-wrap">
                    <h1 class="main-title lg-title lg-title-respon hl-color"><?php the_title( ) ?></h1>
                </div>
                <div class="booking-bar-wrap">
                    <div class="columns">
                        <div class="column">
                            <input type="text" required name="check-in-date" value="<?php echo @$_GET['check-in-date']  ?>" class="f-control js-datepicker" id="from-date" placeholder="<?php echo __('Check in date','monamedia') ?>">
                        </div>
                        <div class="column">
                            <input type="text" required name="check-out-date" value="<?php echo @$_GET['check-out-date']  ?>" class="f-control js-datepicker" id="to-date" placeholder="<?php echo __('Check out date','monamedia') ?>">
                        </div>
                        <div class="column">
                            <!-- <div class="occupancy-setting-wrap">
                                <input type="text" required name="data-booking" class="f-control f-control-user" placeholder="2 người lớn - 0 trẻ em - 1 room" readonly="">
                                <div class="occupancy-setting">  
                                    <div class="sub-menu">
                                        <div class="frow">
                                            <div class="lb">Phòng:</div>
                                            <div class="controls-wrap">
                                                <div class="up-downControl room" data-step="1" data-max="117" data-min="1">
                                                    <a href="javascript:;" class="btn minus">-</a>
                                                    <div class="fcontrol"><span class="value">2</span></div>
                                                    <a href="javascript:;" class="btn plus">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frow">
                                            <div class="lb">Người lớn:</div>
                                            <div class="controls-wrap">
                                                <div class="up-downControl adults" data-step="1" data-max="117" data-min="1">
                                                    <a href="javascript:;" class="btn minus">-</a>
                                                    <div class="fcontrol"><span class="value">4</span>
                                                    </div>
                                                    <a href="javascript:;" class="btn plus">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frow">
                                            <div class="lb">Trẻ em:</div>
                                            <div class="controls-wrap">
                                                <div class="up-downControl children" data-step="1" data-max="117" data-min="0">
                                                    <a href="javascript:;" class="btn minus">-</a>
                                                    <div class="fcontrol"><span class="value">0</span>
                                                    </div>
                                                    <a href="javascript:;" class="btn plus">+</a>
                                                </div>
                                            </div>
                                        </div>
        
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="column">
                            <!-- <button type="submit" class="main-btn sm-btn pill-btn"><?php //echo __('Book Now','monamedia') ?></button> -->
                        </div>
                    </div>
                </div>
                <div class="reservation-wrap"> 
                    <div class="reservation-main">
                        <?php $rooms = get_field('room_choose_booking_page');
                        if( is_array ( $rooms ) ) { 
                            foreach( $rooms as $key =>  $room ) {
                                $roomId = $room['room'];
                                ?> 
                        <input type="hidden" name="id-room[]" value="<?php echo $roomId ?>"> 
                        
                         <section class="reservation-item">
                            <div class="reservation-overview">
                                <div class="reservation-overview-info">
                                    <h2 class="overview-info-title"><?php echo get_the_title($roomId) ?></h2>
                                    <div class="overview-info-desc js-readmore" data-height="60"><?php echo get_the_excerpt( $roomId ) ?></div>
                                    <div class="overview-info-utilities">
                                        <div class="utilities-head">
                                            <?php $serviceCheck = (array) get_field('service_single_room', $roomId); ///var_dump($serviceCheck); ?>
                                            <ul class="utilities-head-nav">
                                                <?php if(in_array('Bathroom_has_shower' , $serviceCheck)  ) {
                                                    ?>
                                                <li class="tooltip">
                                                    <i class="fa fa-shower" aria-hidden="true"></i>
                                                    <div class="tooltip-text">
                                                        <p><?php echo __('Bathroom with a shower','monamedia') ?></p>
                                                    </div>
                                                </li>    
                                                    <?php
                                                }  
                                                if(in_array('wifi' , $serviceCheck)  ) {
                                                    ?>
                                                <li class="tooltip">
                                                    <i class="fa fa-wifi" aria-hidden="true"></i>
                                                    <div class="tooltip-text">
                                                        <p><?php echo __('Internet Wifi access in room','monamedia') ?></p>
                                                    </div>
                                                </li>
                                                <?php
                                                }
                                                if(in_array('air_conditioning' , $serviceCheck)  ) {
                                                ?> 
                                                <li class="tooltip">
                                                    <i class="fa fa-snowflake-o" aria-hidden="true"></i>
                                                    <div class="tooltip-text">
                                                        <p><?php echo __('Air conditioning','monamedia') ?></p>
                                                    </div>
                                                </li>
                                                <?php
                                                }  ?>
                                            </ul>
                                            <a href="#" class="utilities-head-btn"><?php echo __('view all amenities','monamedia') ?></a>
                                        </div>
                                        <?php $service = get_field('service_other_single_room' , $roomId);
                                            if($service != '') {
                                                $serArr = explode(',',$service);
                                                if(is_array($serArr)){ 
                                                    ?>
                                        <div class="utilities-body">
                                            <ul class="utilities-body-nav">
                                                <?php foreach ($serArr as $key => $value) {
                                                    echo "<li>$value</li>";
                                                } ?> 
                                            </ul>
                                        </div>            
                                                    <?php
                                                }    
                                            }  ?> 
                                    </div>
                                    <div class="overview-info-footer">
                                        <?php $bad = get_field('bed_size-single_room' , $roomId);
                                        if($bad != '') {
                                            echo '<span><strong>'. __('Bed size','monamedia').':</strong> '.$bad.'</span>';
                                        }
                                        $roomSize=  get_field('room_size_single_room' , $roomId );
                                        if($roomSize != '' ) {
                                            echo '<span><strong>'. __('Room size','monamedia').' :</strong> '.$roomSize.'</span>';
                                        } 
                                        ?> 
                                    </div>
                                </div>
                                <?php $gallery = get_field('mona_gallery_single' ,$roomId)
                                 ?>
                                <div class="reservation-overview-img">
                                    <?php echo get_the_post_thumbnail( $roomId, '242x182' ) ?>
                                    <?php if( is_array ( $gallery ) ) { 
                                        ?>
                                    <p><?php echo __('Click on image to see more','monamedia') ?></p>
                                    <div class="overview-img-dys" data-thumb='[
                                    <?php 
                                        foreach( $gallery as $key =>  $id ) {
                                            $url = wp_get_attachment_image_url( $id, '1200x600' );
                                            echo '{"src":"'.$url.'","thumb":"'.$url.'", "subHtml": "<h3>'.get_the_title( $roomId ).'</h3>"}';         
                                            if( count($gallery) > $key+1 ) echo ',';
                                        } ?> 
                                    ]'></div>    
                                        <?php
                                    } ?>
                                    
                                </div>
                            </div>
                            <div class="reservation-options">
                                <div class="reservation-options-desc">
                                    <h3 class="reservation-options-desc-title"><?php echo get_the_title($roomId) ?></h3>
                                    <div class="green-color tooltip">
                                        <p><?php echo __('BENEFITS','monamedia') ?>&nbsp;&nbsp;<i class="fa fa-star" aria-hidden="true"></i></p>
                                        <div class="tooltip-text">
                                            <?php echo get_field('content_benefits_room_single', $roomId); ?>
                                        </div>
                                    </div>
                                    <div class="green-color ">
                                        <p><?php echo __('No refund','monamedia') ?>  &nbsp;&nbsp;<i class="fa fa-info-circle" aria-hidden="true"></i></p> 
                                    </div> 
                                </div>
                                <div class="reservation-options-people">
                                    <div class="title tooltip">
                                        <p>
                                            <?php 
                                            $limitUser = min( 10, (int) $room['so_nguoi']);
                                            for( $i = 1; $i <= $limitUser ;$i++) {
                                                echo '<i class="fa fa-user" aria-hidden="true"></i>';
                                            }
                                            ?> 
                                        </p>
                                        <div class="tooltip-text">
                                            <p><strong><?php echo __("Max $limitUser adults", "monamedia") ?></strong></p>
                                            <p><?php echo __('Guests 0 - 5 years old stay for free if using existing bedding.','monamedia') ?> </p>
                                            <p><?php echo __('Extra persons are not available for this room.','monamedia') ?></p>
                                        </div>
                                    </div>
                                    <p class="sub-title"><?php echo $limitUser ?> <?php echo __('adults','monamedia') ?></p>
                                </div>
                                <div class="reservation-options-price">
                                    <div class="new-price tooltip">
                                        <?php $price = $room['price_one_night'] ; ?>
                                        <p>VND <span class="new-price-num" data-price="<?php echo $price ?>"><?php echo  format_money_vnd ( $price )  ?></span></p>
                                        <div class="tooltip-text">
                                            <p><?php echo __('Prices include all local taxes.','monamedia') ?></p>
                                            <p><strong><?php echo __('Base rate','monamedia') ?></strong>: VND <?php echo  format_money_vnd ( $price )  ?></p>
                                        </div>
                                    </div>
                                    <p class="old-price">VND <?php echo format_money_vnd($price) ?></p>
                                    <p class="desc"><?php echo __('per night','monamedia') ?></p>
                                </div>
                                <div class="reservation-options-rooms">
                                    <select class="f-control" name="select-choose-room[]" onchange="selectRoom()">
                                        <?php $maxRoom = min( 10 , $room['max_room']);
                                        for($i = 0 ; $i <= $maxRoom ; $i ++) {
                                            echo "<option value='$i'>$i " . __('rooms', 'monamedia'). "</option>";
                                        }
                                        ?> 
                                    </select>
                                </div>
                            </div> 
                        </section>
                                <?php
                            }
                        }
                        ?> 
                    </div>
                    <div class="reservation-aside">
                        <div class="reservation-aside-sticky">
                            <button type="submit" class="main-btn square-btn blue-btn" id="reservation-aside-btn" disabled><?php echo __('BOOK NOW' , 'monamedia') ?></button>
                            <div class="reservation-aside-total" id="reservation-aside-total" hidden>
                                <p class="total-rooms"><span id="reservation-aside-rooms">0</span> <?php echo __('rooms','monamedia') ?></p>
                                <p class="total-price">VND <span id="reservation-aside-price">0</span></p>
                                <p class="total-night">
                                    <?php echo __('for ' , 'monamedia') ?>
                                    <span id="reservation-aside-date"> 1 </span>  
                                    <?php echo __(' nights' , 'monamedia') ?> 
                                </p><!---->  
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            </form>
        </div>
    </main>
    <div id="popup-info-booking" class="mfp-hide main-popup">
        <h3 class="main-title text-center">Information</h3>
        <form class="ft-contact" id="form-information-reservation">
            <div class="columns">
                <div class="column w-50">
                    <input type="text" name="full-name" class="f-control" placeholder="Full Name">
                </div>
                <div class="column w-50">
                    <input type="email" name="mail-address" class="f-control" placeholder="Email Address">
                </div>
                <div class="column">
                    <input type="tel" name="phone" class="f-control" placeholder="Phone">
                </div> 
                <div class="column">
                    <textarea class="f-control" name="messenger" placeholder="Message"></textarea>
                </div> 
                <div class="column">
                    <button type="submit" class="main-btn sm-btn full-btn mona-has-ajax ">SUBMIT NOW</button>
                </div>
            </div>
            <p class="alert-form"></p>
        </form>
    </div>
    <?php
endwhile;
get_footer();
?>
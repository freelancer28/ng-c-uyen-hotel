<?php
/**
 * Template name: Home Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/gallery.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main"> 
        <div class="home-banner-sec sec">
            <div class="home-banner-video">
               <div class="youtube-video" id="player"></div>
               <div class="video-banner-overlay" id="player-overlay">
                   <img src="<?php echo site_url( 'template' ) ?>/images/logo-2.png" alt="">
               </div>
               <?php $videoId = get_field('id_video_youtube'); ?>
               <script>
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                    var player;
                    function onYouTubeIframeAPIReady() {
                        player = new YT.Player('player', {
                        videoId: '<?php echo $videoId ?>',
                        playerVars: { 'autoplay': 1, 'muted': 1, 'controls': 0, 'loop': 1, 'rel': 0, 'showinfo': 0, 'modestbranding': 0, 'modestbranding': 0},
                        events: {
                            'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                        });
                    }

                    function onPlayerReady(event) {
                        player.mute();
                        player.playVideo();
                        setTimeout(hideOverlay, 7000);
                    }
                    function onPlayerStateChange(event) {
                        if (event.data == YT.PlayerState.ENDED) {
                            player.playVideo();
                        }
                    }
                    function hideOverlay(){
                        const overlay = document.getElementById('player-overlay');
                        overlay.style.transform = 'scale(1.5)';
                        overlay.style.opacity = 0;
                        overlay.addEventListener('transitionend', function handle(){
                            overlay.style.display = "none";
                            overlay.removeEventListener('transitionend', handle);
                        })
                    }
               </script>
            </div>
        </div>
       
        <div class="booking-bar-sec sec">
            <div class="container">
                <form action="<?php echo get_permalink( MONA_BOOKING_NOW ) ?>" method="GET">
                <div class="booking-bar-wrap">
                    <div class="columns">
                        <div class="column">
                            <input type="text" name="check-in-date" class="f-control js-datepicker" id="from-date"
                                   placeholder="Check in date">
                        </div>
                        <div class="column">
                            <input type="text" name="check-out-date" class="f-control js-datepicker" id="to-date"
                                   placeholder="Check out date">
                        </div>
                        <div class="column">
                            <!-- <div class="occupancy-setting-wrap">
                                <input type="text" class="f-control f-control-user"
                                    placeholder="2 người lớn - 0 trẻ em - 1 room" readonly="">
                                <div class="occupancy-setting">
                                    <div class="sub-menu">
                                        <div class="frow">
                                            <div class="lb">Phòng:</div>
                                            <div class="controls-wrap">
                                                <div class="up-downControl room" data-step="1" data-max="117"
                                                     data-min="1">
                                                    <a href="javascript:;" class="btn minus">-</a>
                                                    <div class="fcontrol"><span class="value">2</span></div>
                                                    <a href="javascript:;" class="btn plus">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frow">
                                            <div class="lb">Người lớn:</div>
                                            <div class="controls-wrap">
                                                <div class="up-downControl adults" data-step="1" data-max="117"
                                                     data-min="1">
                                                    <a href="javascript:;" class="btn minus">-</a>
                                                    <div class="fcontrol"><span class="value">4</span>
                                                    </div>
                                                    <a href="javascript:;" class="btn plus">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frow">
                                            <div class="lb">Trẻ em:</div>
                                            <div class="controls-wrap">
                                                <div class="up-downControl children" data-step="1" data-max="117"
                                                     data-min="0">
                                                    <a href="javascript:;" class="btn minus">-</a>
                                                    <div class="fcontrol"><span class="value">0</span>
                                                    </div>
                                                    <a href="javascript:;" class="btn plus">+</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="column">
                            <button type="submit" class="main-btn sm-btn pill-btn white-btn">Book Now</button>
                        </div>
                    </div>
                </div>

                </form>
                
            </div>
        </div>
        <section class="our-story-sec sec">
            <div class="container">
                <div class="our-story-wrap">
                    <div class="main-title-wrap">
                        <h2 class="main-title hl-color"><?php echo get_field('home_st_tt') ?>
                        </h2>
                    </div>
                    <div class="mona-content">
                        <p><?php echo get_field('home_st') ?> </p>

                    </div>
                    <div class="hr-thin"></div>
                    <a href="<?php echo get_field('home_btn_link') ?>"
                       class="readmore-btn"><?php echo get_field('home_btn_text') ?> </a>
                </div>
                <div class="pd-wrap">
                    <div class="columns">
                        <?php
                        $hser = get_field('home_list_ser');
                        if (is_array($hser)) {
                            foreach ($hser as $item) { ?>

                                <div class="column">
                                    <div class="pd-item">
                                        <div class="pd-img">
                                            <a href="<?php echo $item['h_ser_link'] ?>">

                                                <?php
                                                $image = $item['h_image'];
                                                $size = '510x390';
                                                echo wp_get_attachment_image($image, $size); ?>
                                            </a>
                                        </div>
                                        <div class="pd-info">
                                            <h3 class="pd-title"><?php echo $item['h_ser_text'] ?>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                        } ?>


                    </div>
                </div>
            </div>
        </section>
        <section class="offers-sec sec pt-0">
            <div class="container">
                <div class="main-title-wrap text-center">
                    <h2 class="main-title"> <?php echo get_field('') ?></h2>
                </div>
                <div class="offers-wrap">
                    <div class="columns">
                        <?php
                        $h_ofer = get_field('Home_lis_off');
                        if (is_array($h_ofer)) {
                            foreach ($h_ofer as $item) { ?>


                                <div class="column">
                                    <div class="offers-item">
                                        <div class="offers-img">
                                            <div class="offers-img-top">
                                                <a href="<?php echo $item['home_offer_link'] ?>">
                                                    <?php
                                                    $image = $item['home_offer_img'];
                                                    $size = '330x330';
                                                    echo wp_get_attachment_image($image, $size); ?>

                                                </a>
                                            </div>
                                            <div class="offers-img-bottom">
                                                <h3 class="offers-title"><?php echo $item['home_offer_text'] ?></h3>
                                            </div>
                                        </div>
                                        <div class="offers-info">
                                            <div class="hr-thin"></div>
                                            <a href="<?php echo $item['home_offer_link'] ?>"
                                               class="readmore-btn"><?php echo $item['home_offer_text_seemore'] ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="map-sec sec p-0">
            <?php echo get_field('home_map') ?>
        </div>
    </main>
<?php
endwhile;
get_footer();
?>
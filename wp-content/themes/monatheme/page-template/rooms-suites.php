<?php 
/**
 * Template name: Rooms List Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/rooms-suites.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="rooms-intro-sec sec">
            <div class="container">
                <div class="main-title-wrap mb-0">
                    <h1 class="main-title lg-title lg-title-respon hl-color"><?php the_title() ?></h1>
                    <div class="main-desc mona-content"><?php the_content(  ) ?></div>
                </div>
            </div>
        </div>
        <div class="rooms-sec-wrap">
            <?php 
            $count = 6;
            $page = max(1, get_query_var('paged'));
            $offset= ($page-1)*$count;
            $args = array( 
                'post_type'     => 'mona_room',
                'posts_per_page' => $count,
                'page'          =>$page,
                'offset'        =>$offset, 
            );
            $roomQuery = new WP_Query($args);
            while( $roomQuery->have_posts( ) ) {
                $roomQuery->the_post(  );
                ?>
            <section class="rooms-sec sec sec-60 sec-35-respon">
                <div class="container">
                    <div class="rooms-item">
                        <div class="rooms-img">
                            <?php the_post_thumbnail( '1200x600' ) ?>
                        </div>
                        <div class="rooms-info">
                            <h2 class="rooms-title md-title"><?php the_title() ?></h2>
                            <div class="rooms-desc text-wrap"><?php the_excerpt(  ) ?></div>
                            <ul class="rooms-btn">
                                <li>
                                    <div class="hr-thin mt-0"></div>
                                    <a href="<?php the_permalink(  ) ?>" class="readmore-btn"><?php _e('Read more','monamedia') ?></a>
                                </li>
                                <li>
                                    <div class="hr-thin mt-0"></div>
                                    <a href="<?php echo get_permalink( MONA_BOOKING_NOW ) ?>" class="readmore-btn"><?php _e('Book now','monamedia') ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>    
                <?php
            }wp_reset_query(  );  
            ?> 
            <div class="container text-center mb-35">
                <?php mona_page_navi($roomQuery); ?>
            </div>
        </div>
       
    </main>
    <?php
endwhile;
get_footer();
?>
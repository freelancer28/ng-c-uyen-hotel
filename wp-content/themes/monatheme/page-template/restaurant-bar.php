<?php 
/**
 * Template name: Restaurant + Spa Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/restaurant-bar.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="bar-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h1 class="main-title lg-title lg-title-respon hl-color"><?php the_title() ?></h1>
                </div>
                <div class="bar-overview">
                    <div class="bar-overview-slider swiper-init is-slider swiper-pag">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            <?php
                            $gallery = get_field('mona_gallery_single');
                            if( is_array ( $gallery ) ) {
                                foreach( $gallery as $id ) {
                                ?>
                                <div class="swiper-slide">
                                    <?php echo wp_get_attachment_image( $id , '1050x360' ) ?>
                                </div>
                                <?php       
                                } 
                            }   ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <div class="bar-overview-info">
                        <div class="text-wrap text-center">
                            <p><?php the_content() ?></p>
                        </div>
                    </div>
                </div>
                <div class="bar-cate">
                    <?php 
                    $count = 3;
                    $page = max(1, get_query_var('paged'));
                    $offset= ($page-1)*$count;
                    $args = array( 
                        'post_type' => 'mona_restaurant',
                        'posts_per_page' => $count, 
                        'page'  => $page, 
                        'offset' => $offset,
                    );
                    $resQuery = new WP_Query($args);
                    while($resQuery->have_posts() ) {
                        $resQuery->the_post();
                            ?>
                     <section class="bar-cate-item">
                        <div class="columns">
                            <div class="column bar-cate-img">
                                <?php the_post_thumbnail( '510x382' ) ?>
                            </div>
                            <div class="column bar-cate-info">
                                <h2 class="bar-cate-info-title md-title"><?php the_title() ?></h2>
                                <div class="bar-cate-info-desc">
                                    <?php the_excerpt(  ); ?>
                                </div>
                                <div class="bar-cate-info-btn">
                                    <a target="_blank" href="<?php echo get_field('rest_file_pdf') ?>" class="readmore-btn"><?php echo __('View','monamedia') ?> <?php the_title() ?> Menu</a>
                                    <a href="<?php the_permalink(  ) ?>" class="readmore-btn"><?php echo __('Read More','monamedia') ?></a>
                                </div>
                            </div>
                        </div>
                    </section>        
                            <?php
                    }wp_reset_query(  );
                    ?>
                   

                </div>
            </div>
        </div>

        <section class="other-bar-sec sec sec-70 pt-0">
            <div class="container">
                <div class="main-title-wrap text-center">
                    <h2 class="main-title md-title"><?php echo get_field('title_mote_enticing_offsers')   ?></h2>
                </div>
                <div class="other-bar-wrap">
                    <div class="columns columns-3 columns-center">
                        <?php $item = get_field('item_mote_enticing_offsers');
                        if( is_array ( $item ) ) { 
                            foreach( $item as $value ) {
                                 ?>
                         <div class="column">
                            <div class="other-bar-item">
                                <a href="<?php echo $value['url'] ?>">
                                    <?php echo wp_get_attachment_image( $value['image'] , '330x250' ) ?>
                                </a>
                            </div>
                        </div>         
                                 <?php
                            }
                        }
                        ?>
                       
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
endwhile;
get_footer();
?>
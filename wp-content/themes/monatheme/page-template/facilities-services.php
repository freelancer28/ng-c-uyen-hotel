<?php 
/**
 * Template name: Services Page
 * @author : Hùng Nguyễn
 * @html : http://ngocuyenhotel.developerfreelancer.net/template/facilities-services.html
 */
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="rooms-intro-sec sec">
            <div class="container">
                <div class="main-title-wrap mb-0">
                    <h1 class="main-title lg-title lg-title-respon hl-color">Facilities & Services</h1>
                </div>
            </div>
        </div>
        <div class="rooms-sec-wrap">
            <?php 
            $count = 10;
            $page = max(1, get_query_var('paged'));
            $offset= ($page-1)*$count;
            $args = array( 
                'post_type' => 'mona_services' ,
                'posts_per_page' => $count,
                'page' => $page , 
                'offset' => $offset,
            );
            $serviceQuery = new WP_Query($args);
            while($serviceQuery->have_posts() ) {
                $serviceQuery->the_post();
                ?>
            <section class="rooms-sec sec sec-60 sec-35-respon">
                <div class="container">
                    <div class="rooms-item">
                        <div class="rooms-img">
                            <?php the_post_thumbnail( '1200x600' ) ?>
                        </div>
                        <div class="rooms-info">
                            <h2 class="rooms-title md-title"><?php the_title() ?></h2>
                            <div class="rooms-desc mona-content">
                                <?php echo get_field('info_service_single') ?>
                                <?php $content = get_the_content( null, false, get_the_ID() ); ?>
                                <?php if($content != ''){
                                    ?>
                                <a href="<?php the_permalink(  ) ?>" class="readmore-btn"><?php echo __('More information','monamedia') ?></a>
                                    <?php
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>    
                <?php
            }
            ?> 
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>
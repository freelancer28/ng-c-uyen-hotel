<?php
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="room-detail-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h2 class="main-sub-title"><?php _e('Rooms & Suites','monamedia') ?></h2>
                    <h1 class="main-title hl-color"><?php the_title() ?></h1>
                </div>
                <div class="room-detail-img full-img">
                    <?php echo wp_get_attachment_image( get_field('image_banner_single') , '1050x999' ) ?> 
                </div>
                <div class="room-detail-overview">
                    <div class="room-detail-overview-slider">
                        <div class="overview-slider swiper-init is-slider swiper-pag">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php $gallery = get_field('mona_gallery_single');
                                    if( is_array ( $gallery ) ) { 
                                        foreach( $gallery as $id ) {
                                            ?>
                                    <div class="swiper-slide">
                                        <?php echo wp_get_attachment_image( $id , '690x460' ) ?>
                                    </div>        
                                            <?php
                                        }
                                    }
                                    ?>   
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                    <div class="room-detail-overview-content">
                        <div class="text-wrap mona-content">
                            <?php the_content( ) ?>
                        </div>
                    </div>
                </div>
                <div class="room-detail-info text-wrap">
                    <div class="columns">
                        <?php 
                        $title = get_field('title_detail_room_single');
                        $content = get_field('content_detail_room_single');
                        if($title != '' && $content != '') {
                            ?>
                        <div class="column">
                            <p class="room-detail-info-title"><?php echo $title ?></p>
                            <div class="room-detail-info-text mona-content">
                                 <?php echo $content ?>
                            </div>
                        </div>    
                            <?php
                        }
                        ?>
                       
                        <?php 
                            $title = get_field('title_benefits_room_single');
                            $content = get_field('content_benefits_room_single');
                            if($title != '' &&  $content != '') {
                                ?>
                        <div class="column">
                            <p class="room-detail-info-title"><?php echo $title ?></p>
                            <div class="room-detail-info-text">
                                 <?php echo $content ?>
                            </div>
                        </div>        
                                <?php
                            }
                        ?>
                        
                        <div class="column"> <!-- booking -->
                            <p class="room-detail-info-title"><?php _e('Booking','monamedia') ?></p>
                            <div class="room-detail-info-text">
                                <div class="booking-bar-wrap booking-bar-wrap-column"> 
                                    <form action="" class="form-booking-room">  
                                        <input type="hidden" name="name" value="<?php echo get_the_title() ?>">
                                        <input type="hidden" name="url" value="<?php echo get_permalink() ?>">
                                        <input type="hidden" name="type" value="<?php echo __('booking room') ?>">
                                        <div class="columns">
                                            <div class="column">
                                                <input type="text" name="check-in-date" class="f-control js-datepicker" id="from-date" placeholder="<?php _e('Check in date','monamedia') ?>">
                                            </div>
                                            <div class="column">
                                                <input type="text" name="check-out-date" class="f-control js-datepicker" id="to-date" placeholder="<?php _e('Check out date','monamedia') ?>">
                                            </div> 
                                            <div class="column">
                                                <div class="occupancy-setting-wrap occupancy-setting-wrap-vi">
                                                    <input type="text" name="info-room" class="f-control f-control-user" placeholder="2 adults - 0 children - 1 room" readonly="">
                                                    <div class="occupancy-setting">
                                                        <div class="sub-menu">
                                                            <div class="frow">
                                                                <div class="lb"><?php _e('Room','monamedia') ?>:</div>
                                                                <div class="controls-wrap">
                                                                    <div class="up-downControl room" data-step="1" data-max="117" data-min="1">
                                                                        <a href="javascript:;" class="btn minus">-</a>
                                                                        <div class="fcontrol"><span class="value">2</span></div>
                                                                        <a href="javascript:;" class="btn plus">+</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="frow">
                                                                <div class="lb"><?php _e('Adults','monamedia') ?>:</div>
                                                                <div class="controls-wrap">
                                                                    <div class="up-downControl adults" data-step="1" data-max="117" data-min="1">
                                                                        <a href="javascript:;" class="btn minus">-</a>
                                                                        <div class="fcontrol"><span class="value">4</span>
                                                                        </div>
                                                                        <a href="javascript:;" class="btn plus">+</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="frow">
                                                                <div class="lb"><?php _e('Children','monamedia') ?>:</div>
                                                                <div class="controls-wrap">
                                                                    <div class="up-downControl children" data-step="1" data-max="117" data-min="0">
                                                                        <a href="javascript:;" class="btn minus">-</a>
                                                                        <div class="fcontrol"><span class="value">0</span>
                                                                        </div>
                                                                        <a href="javascript:;" class="btn plus">+</a>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="column">
                                                <input type="text" name="customer-phone" class="f-control" placeholder="<?php _e('Your phone','monamedia') ?>">
                                            </div>
                                            <div class="column">
                                                <input type="text" name="customer-name" class="f-control" placeholder="<?php _e('Your name','monamedia') ?>">
                                            </div>
                                            <div class="column">
                                                <input type="email" name="customer-mail" class="f-control" placeholder="<?php _e('Your mail','monamedia') ?>">
                                            </div>
                                            <div class="column">
                                                <button type="submit" class="main-btn sm-btn pill-btn white-btn mona-has-ajax"><?php _e('Book Now','monamedia') ?></button>
                                            </div>
                                            <ul class="alert-form"></ul>
                                        </div> 
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            $postChoose = get_field('choose_room_other_single_room');
            $args = array( 
                'post_type'         => 'mona_room',
                'posts_per_page'    => 5,
                'post__not_in'      => array(get_the_ID()),   
                // 'post__in'          => $postChoose ,   
            ); 
            if(is_array($postChoose) && count($postChoose) > 0){
                $args['post__in'] = $postChoose;
                $args['orderby'] = 'post__in';
            } 
            $orderQuery = new WP_Query($args);
            if($orderQuery->have_posts()){
                ?>
        <section class="order-rooms-sec white-bg sec sec-60 sec-35-respon"> <!-- other room -->
            <div class="container">
                <div class="main-title-wrap text-center">
                    <h2 class="main-title md-title"><?php _e('Other rooms','monamedia') ?></h2>
                </div>
                <div class="order-rooms-wrap">
                    <div class="columns columns-5 columns-center">
                       <?php 
                        while($orderQuery->have_posts()){
                           $orderQuery->the_post();
                            ?>  
                        <div class="column">
                            <div class="offers-item">
                                <div class="offers-img">
                                    <div class="offers-img-top">
                                        <a href="<?php the_permalink(  ) ?>">
                                            <?php the_post_thumbnail( '186x186' ) ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="offers-info">
                                    <h3 class="offers-info-title"><a href="<?php echo get_permalink(  ) ?>"><?php the_title() ?></a></h3>
                                    <div class="hr-thin"></div>
                                    <a href="<?php echo get_permalink(  ) ?>" class="readmore-btn"><?php _e('Read more','monamedia') ?></a>
                                </div>
                            </div>
                        </div>
                            <?php
                        }wp_reset_query(  ); ?> 
                    </div>
                </div>
            </div>
        </section>        
                <?php
            }
        ?>
        
    </main>
    <?php
endwhile;
get_footer();
?>
<?php
function mona_page_navi($wp_query='' , $class='') {
	if($wp_query==''){
	global $wp_query;	
	}
    
    $bignum = 999999999;
    if ($wp_query->max_num_pages <= 1)
        return;
    echo '<nav class="pagination '.$class.'">';
    echo paginate_links(array(
        'base' => str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum))),
        'format' => '',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
        'type' => 'list',
        'end_size' => 3,
        'mid_size' => 3
    ));
    echo '</nav>';
}
function mona_page_navi_url($wp_query = '', $url = '', $class='') {
    if ($wp_query == '') {
        global $wp_query;
    }
    $bignum = 999999999;
    if ($url == '') {
        $url = str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum)));
    }

    if ($wp_query->max_num_pages <= 1)
        return;
    echo '<nav class="pagination '.$class.'">';
    echo paginate_links(array(
        'base' => $url,
        'format' => '',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
        'type' => 'list',
        'end_size' => 3,
        'mid_size' => 3
    ));
    echo '</nav>';
} 
// add_action('init' , 'mona_start_section', 1); // start section 
function mona_start_section()
{
    if(session_id() == '') {
        session_start();
    }
}
 
add_filter('wp_mail_content_type', function( $content_type ) {
    return 'text/html';
});
function format_money_vnd (int $number) { 
    return number_format($number, 0, '', ',');
}
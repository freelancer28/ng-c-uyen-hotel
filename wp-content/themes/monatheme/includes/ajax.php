<?php  
 
// add_action( 'wp_ajax_load_more_all_video', 'load_more_all_video' ); // co dang nhap
// add_action('wp_ajax_nopriv_load_more_all_video', 'load_more_all_video'); // meo dang nhap

// wp_mail($user_email, wp_specialchars_decode(get_bloginfo('name').' Password Reset'), $message)

function send_booking_single_room( ) {
    $data = []; 
    $error = [];
    parse_str($_POST['data'] , $data);
    if ( ! is_array ( $data ) ) { 
       $error[] = "data error";
    } else { 
        $nameOption = strip_tags(@$data['option']) ;

        $checkIn    = strip_tags(@$data['check-in-date']) ;
        $type       = strip_tags(@$data['type']) ;
        $checkOut   = strip_tags(@$data['check-out-date']);
        $info       = strip_tags(@$data['info-room']);
        $url        = strip_tags(@$data['url']);
        $name       = strip_tags(@$data['name']);
        $customerName       = strip_tags(@$data['customer-name']);
        $customerPhone      = strip_tags(@$data['customer-phone']);
        $customerEmail      = strip_tags(@$data['customer-mail']);
        
        if ( $checkIn == '' ) {
            $error[] = __("check in date empty","monamedia");
        }
        if ( $customerEmail == '' ) {
            $error[] = __("Email empty","monamedia");
        }
        if ( $checkOut == '' ) {
            $error[] = __("check out date empty","monamedia");
        }
        if ( $info == '' ) {
            $error[] = __("info room empty","monamedia") ;
        } 
        if ( $customerName == '' ) {
            $error[] = __("Name empty",'monamedia');
        } 
        if ( $customerPhone == '' ) {
            $error[] = __("Phone empty",'monamedia');
        } 
        $adminMail = MONA_ADMIN_MAIL;
        $message .= '<p>Customer name: ' . $customerName . '</p>';
        $message .= '<p>Customer phone: ' . $customerPhone . '</p>';
        $message .= '<p>Customer email: ' . $customerEmail . '</p>';
        $message .= '<p>Check in: ' . $checkIn . '</p>';
        $message .= '<p>Check out: ' . $checkOut . '</p>';
        $message .= '<p>Info: ' . $info . '</p>';
        if( $nameOption != '' ) {
            $message .= '<p>Option: ' . $nameOption . '</p>';
        }
        $message .= "<p>Form: <a href='$url'>$name</a> </p>";
        $sendMail = wp_mail($adminMail, wp_specialchars_decode(get_bloginfo('name') . ' - ' .  $type ), $message);
        if(!$sendMail && count($error) < 1) {
            $error[] = 'Send Mail false';
        }
    }

    if ( count($error) > 0 ) {
        echo json_encode( array( 'status' => 'error' , 'messenger' => json_encode($error) ) );
    } else {
        echo json_encode( array( 'status' => 'success' , 'messenger' => __('Thanks for the reservation, we will contact you','monamedia')) ) ;
    }
   
    wp_die();
}
add_action( 'wp_ajax_send_booking_single_room', 'send_booking_single_room' ); // co dang nhap
add_action('wp_ajax_nopriv_send_booking_single_room', 'send_booking_single_room'); // meo dang nhap

function submit_form_reservation() {
    $data = [];
    parse_str(@$_POST['data'], $data);
    if(is_array($data)){ 
        $error = [];

        $fullName       =   strip_tags(@$data['full-name']);
        $mail           =   strip_tags(@$data['mail-address']);
        $phone          =   strip_tags(@$data['phone']);
        $messenger      =   strip_tags(@$data['messenger']); 
        //
        $checkIn        =   strip_tags(@$data['check-in-date']);
        $checkOut       =   strip_tags(@$data['check-out-date']);
        $idRoom         =   @$data['id-room']; 
        $select         =   @$data['select-choose-room'];
        $priceArray     =   get_field('room_choose_booking_page' ,MONA_BOOKING_NOW);
        $roomArray = [];
        foreach ($select as $key => $value ) {
            if($value > 0 ){
                $roomArray[] = array( 'id-room' => $idRoom[$key] , 'book' => $value , 'price' => $priceArray[$key]['price_one_night'] ) ;
            } 
        }
        // var_dump($roomArray);
        if ( $checkIn == '' ) {
            $error[] = __("Check in date empty","monamedia");
        }
        if( $checkOut  == '' ) {
            $error[] = __("Check out date empty","monamedia");
        }
        $adminMail = MONA_ADMIN_MAIL;
        $message .= '<p>Customer name: ' . $fullName . '</p>';
        $message .= '<p>Customer phone: ' . $phone . '</p>';
        $message .= '<p>Customer email: ' . $mail . '</p>';
        $message .= '<p>Check in: ' . $checkIn . '</p>';
        $message .= '<p>Check out: ' . $checkOut . '</p>';
        $message .= '<p>Info: ' . $messenger . '</p>';
        if( count( $roomArray ) > 0 ) {
            $message .=  '<ul>';
            foreach( $roomArray as $k => $item ) {
                $message .= '<li>
                <span>'.($k+1).'</span>
                <a href="'.get_permalink( $item['id-room'] ).'"> '.get_the_title( $item['id-room']).'</a> 
                | Room: '.$item['book'].' 
                | price: '.format_money_vnd($item['price']).' / per night  
                </li>';
            }
            $message .= '</ul>';
        }  
        // var_dump($message);
        $sendMail = wp_mail($adminMail, wp_specialchars_decode(get_bloginfo('name') . ' - ' .  'booking reservation' ), $message);
        if(!$sendMail && count($error) < 1) {
            $error[] = 'Send Mail false';
        }
        if ( count($error) > 0 ) {
            echo json_encode( array( 'status' => 'error' , 'messenger' => json_encode($error) ) );
        } else {
            echo json_encode( array( 'status' => 'success' , 'messenger' => __('Thanks for the reservation, we will contact you','monamedia')) ) ;
        }
    }   
    wp_die();
}
add_action( 'wp_ajax_submit_form_reservation', 'submit_form_reservation' ); // co dang nhap
add_action('wp_ajax_nopriv_submit_form_reservation', 'submit_form_reservation'); // meo dang nhap
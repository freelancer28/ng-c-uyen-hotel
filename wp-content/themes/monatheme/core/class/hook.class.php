<?php

class Mona_hook {

    public function __construct() {
        add_filter('pre_get_posts', [$this, 'prefix_limit_post_types_in_search']);
        add_filter('wpcf7_autop_or_not', '__return_false');
    }

    public function prefix_limit_post_types_in_search($query) {
        if (!is_admin()) {
            $query->set('ignore_sticky_posts', true);
        }
        if( is_tax('playlist_video_cate') && $query->is_main_query(  ) ) {
            $post_type = (array) $query->get('post_type'.true);
          
            $post_type[]='mona_post_video';
            $query->set('post_type', $post_type);
            $query->set('post_status',array( 'publish' ));
            $query->set('posts_per_page', 12 ); //MONA_NAVIGATION_COUNT

        }
        if( is_tax('custom_list_video_cate') && $query->is_main_query(  ) ) {
            $post_type = (array) $query->get('post_type'.true);
          
            $post_type[]='mona_post_video';
            $query->set('post_type', $post_type);
            $query->set('post_status',array( 'publish' ));
            $query->set('posts_per_page', 12 ); //MONA_NAVIGATION_COUNT

        }
        return $query;
    }

}

new Mona_hook();

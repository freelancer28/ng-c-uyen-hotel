<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @author : monamedia
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <!-- Meta
                ================================================== -->
        <meta charset="<?php bloginfo('charset'); ?>">
            <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width">
                <?php wp_site_icon(); ?>
                <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
                <link rel="stylesheet" href="<?php echo get_site_url() ?>/template/css/style.css"/>
                <!-- <link rel="stylesheet" href="<?php //echo get_site_url() ?>/template/css/responsive.css" media="all"/> -->
                <?php wp_head(); ?>
                </head>
                    <?php
    if(wp_is_mobile()){
        $body = 'mobile-detect';
    }else{
        $body = 'desktop-detect'; 
    }
    ?>
<body <?php body_class($body); ?>> 
    <div id="page-loading" class="page-loading"><div></div></div>
    <header id="header" class="header">
        <div class="top-header">
            <div class="container">
                <ul class="language-picker">
                    <li><a href="#">Tiếng Việt</a></li>
                    <li class="active"><a href="#">English</a></li>
                    <li><a href="#">Korean</a></li>
                </ul>
                <a href="<?php echo get_permalink(  MONA_BOOKING_NOW ) ?>" class="main-btn booknow-btn">Book Now</a>
            </div>
        </div>
        <div class="bottom-header">
            <div class="container">
                <div class="hd-logo">
                    <?php echo get_custom_logo(); ?>
                </div>
                <div class="main-menu" id="main-menu">
                    <div class="main-menu-inner">
                        <div class="main-menu-close" id="main-menu-close">
                            <span></span>
                        </div> 
                        <?php  
                        wp_nav_menu(array(
                            'container' => false,
                            'container_class' => 'nav-ul',
                            'menu_class' => 'main-menu-nav',
                            'theme_location' => 'primary-menu',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'fallback_cb' => false,
                            'walker' => new Mona_Custom_Walker_Nav_Menu,
                        ));
                        ?>
                        <ul class="language-picker main-menu-language-picker">
                            <li><a href="#">Tiếng Việt</a></li>
                            <li class="active"><a href="#">English</a></li>
                            <li><a href="#">Korean</a></li>
                        </ul>
                    </div>
                    <div class="main-menu-overlay" id="main-menu-overlay"></div>
                </div>
                <ul class="language-picker">
                    <li><a href="#">Tiếng Việt</a></li>
                    <li class="active"><a href="#">English</a></li>
                    <li><a href="#">Korean</a></li>
                </ul>
                <a href="#" class="main-btn booknow-btn">Book Now</a>
                <div class="main-menu-btn" id="main-menu-btn">
                    <div class="bar"></div>
                </div>
            </div>
        </div>
    </header>
  
   
 
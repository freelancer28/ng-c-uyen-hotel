jQuery(document).ready(function($){

    const sendAjaxPromise = (url, type, data) => new Promise((resolve, reject) => { 
        $.ajax({
            url: url,
            type: type,
            data: data, 
            success: function (result) {
                resolve(result);
            },
            error: function (error) {
                reject(error);
            }
        });
    }); 
    $('.form-booking-room').on('submit', async function(e) {
        e.preventDefault();
        let $this = $(this),
            $formData = $this.serialize(), $alert = $this.find('.alert-form'),
            $loading = $this.find('.mona-has-ajax')
        ;
        $loading.addClass('loading');
        try {  
            const result = await sendAjaxPromise(mona_ajax_url.ajaxURL, 'post', 
                {
                    action: 'send_booking_single_room', 
                    data: $formData,
                }
            ); 
            $loading.removeClass('loading');
            let $result = JSON.parse(result);
            if ( $result.status == 'success')  { 
                $this.trigger("reset"); 
                $alert.html($result.messenger);
                $alert.css('color' , 'green');
            } else {
                let $messenger = JSON.parse($result.messenger),
                    $html = '' ;  
                if ( $messenger.length > 0 ) {
                    for ( let i = 0 ; i < $messenger.length ; i++ ) {
                        $html += `<li>${$messenger[i]}</li>`
                    } 
                    $alert.html($html);
                    $alert.css('color' , 'red');
                }
            } 
        } catch (error) {
            $loading.removeClass('loading');
            console.log('mona_ajax_call_mfa error:', error);
        }  
           
    });
    $('.open-popup-offer').magnificPopup({
        removalDelay: 500,
        callbacks: {
            beforeOpen: function() { 
                this.st.mainClass = "mfp-zoom-in";
            },
        },
        midClick: true
    });
    $('.open-popup-offer').on('click',function(e){
        let name = $(this).attr('data-name');
        $('.name-offer input').val(name); 
    });
    $('.form-reservation').on('submit' , function(e){
        e.preventDefault(); 
        $.magnificPopup.open({
            items: {
                src: $('#popup-info-booking'),
            },
            disableOn: 768,
            mainClass: 'mfp-zoom-in',
            removalDelay: 500,
            preloader: false,
            fixedContentPos: true,
            callbacks: {
                
            }
        }, 0); 
    });
    $('#form-information-reservation').on('submit', async function(e){
        e.preventDefault();
        let $data = $('#form-information-reservation').serialize() + '&' + $('.form-reservation').serialize() , 
            $loading = $(this).find('button') ,
            $alert  = $(this).find('.alert-form')
            ;
        try {  
            $loading.addClass('loading');
            const result = await sendAjaxPromise(mona_ajax_url.ajaxURL, 'post', 
                {
                    action: 'submit_form_reservation', 
                    data: $data,
                }
            ); 
            $loading.removeClass('loading');
            let $result = JSON.parse( result );
            if($result.status == 'success') {
                $alert.html($result.messenger);
                $alert.css('color' , 'green');
            } else {
                let $messenger = JSON.parse($result.messenger),
                    $html = '' ;  
                if ( $messenger.length > 0 ) {
                    for ( let i = 0 ; i < $messenger.length ; i++ ) {
                        $html += `<li>${$messenger[i]}</li>`
                    } 
                    $alert.html($html);
                    $alert.css('color' , 'red');
                }
            }
        } catch (error) {
            $loading.removeClass('loading');
            console.log('mona_ajax_call_mfa error:', error);
        }  
    });

});

    <footer class="footer">
        <div class="top-footer">
            <div class="container">
                <?php get_template_part('patch/contact')?>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="container">
                <p><?php echo mona_get_option('mona_coppyright'); ?></p>
            </div>
        </div>
    </footer>
    <div id="scroll-top" class="scroll-top"><i class="fa fa-angle-up"></i></div>
    <!-- <script src="js/jquery-1.12.4.min.js"></script> -->
    <script src="<?php echo site_url() ?>/template/js/Magnific-Popup-master/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo site_url() ?>/template/js/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?php echo site_url() ?>/template/js/swiper-5.4.3/package/js/swiper.min.js"></script>
    <script src="<?php echo site_url() ?>/template/js/flatpickr/flatpickr.js"></script>
    <script src="<?php echo site_url() ?>/template/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
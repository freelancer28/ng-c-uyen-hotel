<main id="main" class="main">
    <div class="room-detail-sec sec sec-70">
        <div class="container">
            <div class="main-title-wrap">
                <h2 class="main-sub-title">Restaurant & Bar</h2>
                <h1 class="main-title hl-color">Pool Bar</h1>
            </div>
            <div class="room-detail-img full-img">
                <img src="images/room-detail-2.jpg" alt="">
            </div>
            <div class="room-detail-info text-wrap">
                <div class="columns">
                    <div class="column">
                        <div class="hr-thin mt-0"></div>
                        <a href="#" class="readmore-btn">View Menu</a>
                    </div>
                    <div class="column">
                        <p class="room-detail-info-title">Information</p>
                        <div class="room-detail-info-text">
                            <p>Located amidst beautiful green and airy sky, Pool Bar is your spot to unwind after a day of historical, cultural intakes of Hoian sightseeing or a simple weekend getaway. With the cool breeze of central Vietnam, our pool awaits you for a dip in the refreshing water and sip away our signature Anio Boutique Cocktail and Anio Passion Mocktail. Yet, the exquisite lounge at pool-water level is your promise to catch the glory glimpse of the glittering sun or enjoy the perfect Hoi An Sunset cocktail at the seductive sundown.</p>
                        </div>
                    </div>
                    <div class="column">
                        <p class="room-detail-info-title">Opening time</p>
                        <div class="room-detail-info-text">
                            <p><strong>From:</strong>     6:30 am – 21:30 pm</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="room-detail-gallery">
                <div class="room-detail-gallery-img">
                    <img src="images/gl-1.jpg" alt="">
                </div>
                <div class="room-detail-gallery-img">
                    <img src="images/gl-2.png" alt="">
                </div>
                <div class="room-detail-gallery-img">
                    <img src="images/gl-3.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <section class="order-rooms-sec white-bg sec sec-60 sec-35-respon">
        <div class="container">
            <div class="main-title-wrap text-center">
                <h2 class="main-title md-title">Other rooms</h2>
            </div>
            <div class="order-rooms-wrap">
                <div class="columns columns-4 columns-center">
                    <div class="column">
                        <div class="offers-item">
                            <div class="offers-img">
                                <div class="offers-img-top">
                                    <a href="#">
                                        <img src="images/offers-1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="offers-info">
                                <h3 class="offers-info-title"><a href="#">Junior Suite</a></h3>
                                <div class="hr-thin"></div>
                                <a href="#" class="readmore-btn">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="offers-item">
                            <div class="offers-img">
                                <div class="offers-img-top">
                                    <a href="#">
                                        <img src="images/offers-2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="offers-info">
                                <h3 class="offers-info-title"><a href="#">Junior Suite</a></h3>
                                <div class="hr-thin"></div>
                                <a href="#" class="readmore-btn">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
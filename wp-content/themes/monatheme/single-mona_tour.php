<?php
get_header();
while (have_posts()):
    the_post();
    ?>
    <main id="main" class="main">
        <div class="bar-sec sec sec-70">
            <div class="container">
                <div class="main-title-wrap">
                    <h2 class="main-sub-title">Tours</h2>
                    <h1 class="main-title hl-color">Hue – Hoi An daily tours</h1>
                </div>
                <div class="bar-overview">
                    <div class="bar-overview-slider swiper-init is-slider swiper-pag">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php $slider = get_field('gallery_banner_slider_single_tour');
                                if( is_array ( $slider ) ) { 
                                    foreach( $slider as $idImage ) {
                                         ?>
                                <div class="swiper-slide">
                                    <?php echo wp_get_attachment_image( $idImage , '1050x360' ) ?>
                                </div>         
                                         <?php
                                    }
                                }
                                ?>  
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="bar-faqs"> <!--active-->
                    <?php $active = 'active';
                    $content = get_field('item_content_single_tour');
                    if( is_array ( $content  ) ) { 
                        foreach( $content  as $itemContent ) {
                            ?>
                    <div class="bar-faqs-item <?php echo $active ?>">
                        <h3 class="bar-faqs-title"><span></span><?php echo $itemContent['title'] ?></h3>
                        <div class="bar-faqs-info">
                            <section class="s-offers-sec">
                                <div class="s-offers-img">
                                    <div class="s-offers-img-top">
                                        <?php echo wp_get_attachment_image( $itemContent['image'] , '1010x380' ) // <img src="images/s-offers-2.jpg" alt=""> ?> 
                                    </div>
                                </div>
                                <div class="s-offers-info">
                                    <div class="s-offers-desc text-wrap">
                                        <?php echo $itemContent['content'] ?>
                                    </div>
                                    <div class="s-offers-desc text-wrap">
                                        <div class="columns">
                                            <?php $columns =$itemContent['column_item'];
                                            if( is_array ( $columns ) ) { 
                                                foreach( $columns as $column ) {
                                                    ?>
                                            <div class="column">
                                                <p class="s-offers-desc-title"><?php echo $column['title'] ?></p>
                                                <div class="s-offers-desc-text">
                                                    <?php echo $column['content'] ?>
                                                </div>
                                            </div>        
                                                    <?php
                                                }
                                            }
                                            ?> 
                                            <?php $price = $itemContent['price'];
                                            if(is_array($price)){
                                                ?>
                                            <div class="column w-66">
                                                <p class="s-offers-desc-title"><?php echo $itemContent['title_price'] ?></p>
                                                <div class="s-offers-desc-text">
                                                    <div class="custom-table">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <?php 
                                                                    foreach($price as $key => $item ) {
                                                                        $title = $item['title'];
                                                                        if($key == 0) {
                                                                            echo "<th>$title</th>";
                                                                        }else {
                                                                            echo "<td>$title</td>";
                                                                        }
                                                                    }
                                                                    ?>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <?php  
                                                                    foreach($price as $key => $item ) {
                                                                        $title = $item['price'];
                                                                        if($key == 0) {
                                                                            echo "<th>$title</th>";
                                                                        }else {
                                                                            echo "<td>$title</td>";
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>    
                                                <?php
                                            }
                                            ?> 
                                            <div class="column">
                                                <p class="s-offers-desc-title">Booking</p>
                                                <div class="room-detail-info-text">
                                                    <div class="booking-bar-wrap booking-bar-wrap-column">
                                                    <form action="" class="form-booking-room">  
                                                        <input type="hidden" name="name" value="<?php echo get_the_title() ?>">
                                                        <input type="hidden" name="url" value="<?php echo get_permalink() ?>">
                                                        <input type="hidden" name="option" value="<?php echo $itemContent['title'] ?>">
                                                        <input type="hidden" name="type" value="<?php echo __('booking tour') ?>">
                                                        <div class="columns">
                                                            <div class="column">
                                                                <input type="text" name="check-in-date" class="f-control js-datepicker" id="from-date" placeholder="<?php _e('Check in date','monamedia') ?>">
                                                            </div>
                                                            <div class="column">
                                                                <input type="text" name="check-out-date" class="f-control js-datepicker" id="to-date" placeholder="<?php _e('Check out date','monamedia') ?>">
                                                            </div> 
                                                            <div class="column">
                                                                <div class="occupancy-setting-wrap occupancy-setting-wrap-vi">
                                                                    <input type="text" name="info-room" class="f-control f-control-user" placeholder="2 adults - 0 children - 1 room" readonly="">
                                                                    <div class="occupancy-setting">
                                                                        <div class="sub-menu">
                                                                            <div class="frow">
                                                                                <div class="lb"><?php _e('Room','monamedia') ?>:</div>
                                                                                <div class="controls-wrap">
                                                                                    <div class="up-downControl room" data-step="1" data-max="117" data-min="1">
                                                                                        <a href="javascript:;" class="btn minus">-</a>
                                                                                        <div class="fcontrol"><span class="value">2</span></div>
                                                                                        <a href="javascript:;" class="btn plus">+</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="frow">
                                                                                <div class="lb"><?php _e('Adults','monamedia') ?>:</div>
                                                                                <div class="controls-wrap">
                                                                                    <div class="up-downControl adults" data-step="1" data-max="117" data-min="1">
                                                                                        <a href="javascript:;" class="btn minus">-</a>
                                                                                        <div class="fcontrol"><span class="value">4</span>
                                                                                        </div>
                                                                                        <a href="javascript:;" class="btn plus">+</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="frow">
                                                                                <div class="lb"><?php _e('Children','monamedia') ?>:</div>
                                                                                <div class="controls-wrap">
                                                                                    <div class="up-downControl children" data-step="1" data-max="117" data-min="0">
                                                                                        <a href="javascript:;" class="btn minus">-</a>
                                                                                        <div class="fcontrol"><span class="value">0</span>
                                                                                        </div>
                                                                                        <a href="javascript:;" class="btn plus">+</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="column">
                                                                <input type="text" name="customer-phone" class="f-control" placeholder="<?php _e('Your phone','monamedia') ?>">
                                                            </div>
                                                            <div class="column">
                                                                <input type="text" name="customer-name" class="f-control" placeholder="<?php _e('Your name','monamedia') ?>">
                                                            </div>
                                                            <div class="column">
                                                                <input type="email" name="customer-mail" class="f-control" placeholder="<?php _e('Your mail','monamedia') ?>">
                                                            </div>
                                                            <div class="column">
                                                                <button type="submit" class="main-btn sm-btn pill-btn white-btn mona-has-ajax"><?php _e('Book Now','monamedia') ?></button>
                                                            </div>
                                                            <ul class="alert-form"></ul>
                                                        </div> 
                                                    </form> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>        
                            <?php $active = ''; 
                        }
                    }
                    ?>
                    
                     
                </div>
            </div>
        </div>
        <?php $tourRelated = get_field('choose_2_tour_single_tour');
        if(is_array($tourRelated)){ 
            ?>
        <section class="order-rooms-sec sec sec-70 pt-0">
            <div class="container">
                <div class="white-bg-wrap">
                    <div class="main-title-wrap text-center">
                        <h2 class="main-title md-title"><?php echo __('Other tours', 'monamedia'); ?></h2>
                    </div>
                    <div class="order-rooms-wrap">
                        <div class="columns columns-2 columns-center">
                            <?php  $args = array( 
                                'post_type' => 'mona_tour',
                                'posts_per_page' => 2, 
                                'post__in' => $tourRelated,
                            );
                            $queryRelated = new WP_Query($args);
                            while($queryRelated->have_posts()) {
                                $queryRelated->the_post();
                                ?>
                            <div class="column">
                                <div class="offers-item">
                                    <div class="offers-img">
                                        <div class="offers-img-top">
                                            <a href="<?php the_permalink(  ) ?>">
                                                <?php the_post_thumbnail( '500x500' ) ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="offers-info">
                                        <h3 class="offers-info-title"><a href="<?php the_permalink(  ) ?>"><?php the_title() ?></a></h3>
                                        <div class="hr-thin"></div>
                                        <a href="<?php the_permalink(  ) ?>" class="readmore-btn"><?php echo __('Read more','monamedia') ?></a>
                                    </div>
                                </div>
                            </div>   
                                <?php
                            }wp_reset_query(  );
                            ?>
                           
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>    
            <?php
        }
         ?>
        
    </main>
    <?php
endwhile;
get_footer();
?>
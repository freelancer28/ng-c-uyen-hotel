if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

function isHidden(elem) {
    return !elem.offsetWidth && !elem.offsetHeight;
}

function scrollToTop() {
    if (pageYOffset > 0) {
        window.scrollTo(0, pageYOffset - pageYOffset / 8);
        requestAnimationFrame(scrollToTop);
    }
}
window.addEventListener('DOMContentLoaded', () => {
    document.addEventListener('click', e => {
        if (e.target.closest('.tab-link')) {
            const tabLink = e.target.closest('.tab-link');
            const tabLinkSilblings = tabLink.parentElement.children;
            const tabContent = document.querySelector(tabLink.getAttribute('data-tab'));
            const tabContentSilblings = tabContent.parentElement.children;
            for (let i = 0; i < tabLinkSilblings.length; i++) {
                if (tabLinkSilblings[i].classList.contains('active')) tabLinkSilblings[i].classList.remove('active');
            }
            tabLink.classList.add('active');
            for (let i = 0; i < tabContentSilblings.length; i++) {
                if (tabContentSilblings[i].classList.contains('active')) tabContentSilblings[i].classList.remove('active');
            }
            tabContent.classList.add('active');
        }
    });
    document.querySelectorAll('.tab-link.active').forEach(el => el.click());

    document.querySelectorAll('.main-menu-nav .dropdown').forEach(el => {
        const dropdown = el;
        const arrows = document.createElement("span");
        arrows.classList.add('sub-menu-wrap-toggle');
        dropdown.querySelector('a').insertAdjacentElement('afterend', arrows);
        arrows.addEventListener('click', e => {
            e.preventDefault();
            dropdown.classList.toggle('show-sub-menu');
        });
    });

    if (document.getElementById('main-menu-btn') && document.getElementById('main-menu')) {
        const mainMenuBtn = document.getElementById('main-menu-btn');
        const mainMenu = document.getElementById('main-menu');
        const mainMenuOverlay = document.getElementById('main-menu-overlay');
        const mainMenuCloseBtn = document.getElementById('main-menu-close');
        mainMenuBtn.addEventListener('click', e => {
            mainMenu.classList.toggle('active');
            document.body.style.overflow = "hidden";
        })
        if (mainMenuOverlay) {
            mainMenuOverlay.addEventListener('click', e => {
                mainMenu.classList.remove('active');
                document.body.style.overflow = "visible";
            });
        }
        if (mainMenuCloseBtn) {
            mainMenuCloseBtn.addEventListener('click', e => {
                mainMenu.classList.remove('active');
                document.body.style.overflow = "visible";
            });
        }
    }

    if (document.getElementById('scroll-top')) {
        const scrollTopBtn = document.getElementById('scroll-top');
        pageYOffset >= 100 ? scrollTopBtn.classList.add('show') : scrollTopBtn.classList.remove('show');
        window.addEventListener('scroll', e => {
            pageYOffset >= 100 ? scrollTopBtn.classList.add('show') : scrollTopBtn.classList.remove('show');
        });
        scrollTopBtn.addEventListener('click', scrollToTop);
    }

    // MONA CONTENT
    document.querySelectorAll('.mona-content iframe[src^="https://www.youtube.com/"]').forEach(el => {
        const wrap = document.createElement("div");
        wrap.classList.add('mona-youtube-wrap');
        el.insertAdjacentElement("afterend", wrap);
        wrap.appendChild(el);
    });

    document.querySelectorAll('.mona-content table').forEach(el => {
        const wrap = document.createElement("div");
        wrap.classList.add('mona-table-wrap');
        el.insertAdjacentElement("afterend", wrap);
        wrap.appendChild(el);
    });

    document.querySelectorAll('.swiper-init.is-slider').forEach(el => {
        const slider = el.querySelector('.swiper-container');
        const pagination = el.querySelector('.swiper-pagination');
        const prevBtn = el.querySelector('.swiper-button-prev');
        const nextBtn = el.querySelector('.swiper-button-next');
        new Swiper(slider, {
            speed: 1200,
            autoHeight: false,
            effect: 'fade',
            slidesPerView: 'auto',
            fadeEffect: {
                crossFade: true
            },
            autoplay: {
                speed: 6000,
            },
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextBtn,
                prevEl: prevBtn,
            },
            loop: true,
        });
    });

    document.querySelectorAll('.booking-bar-wrap').forEach(el => {
        const fromDateEl = el.querySelector('#from-date');
        const toDateEl = el.querySelector('#to-date');
        if (toDateEl && fromDateEl) {
            const toDate = flatpickr(toDateEl, {
                disableMobile: "true",
                minDate: "today",
                onChange: function(){
                    document.getElementById('reservation-aside-total') && selectRoom();
                }
            });
            const fromDate = flatpickr(fromDateEl, {
                disableMobile: "true",
                minDate: "today",
                onChange: function(selectedDates, dateStr, instance) {
                    document.getElementById('reservation-aside-total') && selectRoom();
                    toDate.set('minDate', dateStr)
                },
            });
        }
    });

    document.addEventListener('click', e => {
        if (e.target.closest('.utilities-head-btn')) {
            e.preventDefault();
            e.target.closest('.overview-info-utilities').querySelector('.utilities-body').classList.toggle('active');
        }
    });

    //READMORE
    function jsReadmore(el) {
        const maxHeight = el.getAttribute('data-height');
        if (el.offsetHeight > parseInt(maxHeight)) {
            const wrap = document.createElement("div");
            wrap.classList.add('js-readmore-content');
            wrap.innerHTML = el.innerHTML;
            wrap.style.maxHeight = `${maxHeight}px`;
            el.innerHTML = '';
            el.insertAdjacentElement('afterbegin', wrap);

            const btn = document.createElement("a");
            btn.classList.add('js-readmore-btn');
            btn.textContent = '...more details';
            el.insertAdjacentElement('beforeend', btn);

            btn.addEventListener('click', e => {
                e.preventDefault();
                if (btn.classList.contains('active')) {
                    btn.classList.remove('active');
                    btn.textContent = '...more details';
                    wrap.style.maxHeight = `${maxHeight}px`;
                } else {
                    btn.classList.add('active');
                    btn.textContent = '...less details';
                    wrap.style.maxHeight = `${wrap.scrollHeight}px`;
                }
            });
        }
    }

    window.addEventListener('load', e => {
        document.querySelectorAll('.js-readmore').forEach(el => {
            jsReadmore(el);
        });
    })

    const observer = new MutationObserver(mutations => {
        for (let mutation of mutations) {
            for (let node of mutation.addedNodes) {
                if (!(node instanceof HTMLElement)) continue;
                if (!node.matches('.js-readmore')) continue;
                jsReadmore(node);
            }
        }
    });
    observer.observe(document, {
        childList: true,
        subtree: true
    });
});

window.addEventListener('load', () => {
    if (document.querySelector('#header .bottom-header') && document.getElementById('main')) {
        const header = document.querySelector('#header .bottom-header');
        const main = document.getElementById('main');
        const headerHeight = header.offsetHeight;
        const headerOffset = header.offsetTop;
        if (pageYOffset > headerOffset) {
            header.classList.add('fixed');
            main.style.marginTop = headerHeight + 'px';
        } else {
            header.classList.remove('fixed');
            main.style.marginTop = '';
        }
        window.addEventListener('scroll', function() {
            if (pageYOffset > headerOffset) {
                header.classList.add('fixed');
                main.style.marginTop = headerHeight + 'px';
            } else {
                header.classList.remove('fixed');
                main.style.marginTop = '';
            }
        })
    }
    if (document.getElementById('page-loading')) {
        const loading = document.getElementById('page-loading');
        loading.classList.add('loaded');
    }
})
jQuery(document).ready(function($) {
    $('.open-popup-btn').magnificPopup({
        removalDelay: 500,
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = "mfp-zoom-in";
            },
        },
        midClick: true
    });
    $('.open-video-btn').magnificPopup({
        disableOn: 768,
        type: 'iframe',
        mainClass: 'mfp-zoom-in',
        removalDelay: 500,
        preloader: false,
        fixedContentPos: false,
    });

    $(".up-downControl").each(function() {
        let $this = $(this);
        let step = parseInt($this.attr("data-step"));
        let min = parseInt($this.attr("data-min"));
        let max = parseInt($this.attr("data-max"));
        let $occBlock = $this.closest(".occupancy-setting-wrap");
        function updateValue() {
            let value = parseInt($this.find(".value").text());
            if ($(this).hasClass("minus")) {
                value = value - step;
                if (value < min) return;
            }
            if ($(this).hasClass("plus")) {
                value = value + step;
                if (value > max) return;
            }
            $this.find(".value").text(value);
            let roms = $occBlock.find(".up-downControl.room").find(".value").text();
            let auts = parseInt(
                $occBlock.find(".up-downControl.adults").find(".value").text()
            );
            let chrn = parseInt(
                $occBlock.find(".up-downControl.children").find(".value").text()
            );
            let romsTXT = "";
            if ($occBlock.find('.f-control-user').length) {
                if($occBlock.hasClass('occupancy-setting-wrap-vi')){
                    romsTXT = roms != "" ? parseInt(roms) + " phòng " : "";
                    $occBlock.find('.f-control-user').val(romsTXT + auts + " người lớn " + chrn + " trẻ em");
                }
                else{
                    romsTXT = roms != "" ? parseInt(roms) + " room(s) " : "";
                    $occBlock.find('.f-control-user').val(romsTXT + auts + " adult(s) " + chrn + " children");
                }
            }
        }
        updateValue();
        $this.find(".btn").on("click", updateValue);
    });

    $('.occupancy-setting-wrap .f-control-user').on('click', function(e) {
        $(this).closest('.occupancy-setting-wrap').toggleClass('active');
    });

    $('body').on('click', function(e) {
        if (e.target.closest('.occupancy-setting-wrap')) return;
        $('.occupancy-setting-wrap').removeClass('active');
    });

    $(document).on("click", ".gallery-item", function(e) {
        e.preventDefault();
        let $thumb = $(this).find(".gallery-dys").attr("data-thumb");
        $thumb = $.parseJSON($thumb);
        if ($thumb) {
            $(this).lightGallery({
                share: false,
                actualSize: false,
                download: false,
                autoplayControls: false,
                dynamic: true,
                dynamicEl: $thumb,
                thumbnail: true,
                animateThumb: true,
                showThumbByDefault: true,
            });
        }
    });

    $(document).on("click", ".reservation-overview-img img", function(e) {
        e.preventDefault();
        let $thumb = $(this).siblings(".overview-img-dys").attr("data-thumb");
        $thumb = $.parseJSON($thumb);
        if ($thumb) {
            $(this).lightGallery({
                share: false,
                actualSize: false,
                download: false,
                autoplayControls: false,
                dynamic: true,
                dynamicEl: $thumb,
                thumbnail: true,
                animateThumb: true,
                showThumbByDefault: true,
            });
        }
    });

    if ($('.bar-faqs-item.active').length) {
        $('.bar-faqs-item.active').find('.bar-faqs-info').show();
    }
    $(document).on('click', '.bar-faqs-title', function() {
        $(this).closest('.bar-faqs-item').siblings('.bar-faqs-item').removeClass('active').find('.bar-faqs-info').slideUp();
        $(this).closest('.bar-faqs-item').addClass('active').find('.bar-faqs-info').stop().slideDown();
    });
});

// function selectRoom(current, limit, price){
//     const $ = jQuery.noConflict();
//     let val = 0;
//     const currentName = $(current).attr('name');
//     const currentSiblings = $(`[name="${currentName}"]`);
//     currentSiblings.each((index, el) =>{
//         val += parseInt($(el).val());
//     });
//     currentSiblings.each((index, el) => {
//         const elVal = parseInt($(el).val());
//         $(el).find(`option`).show();
//         $(el).find(`option:gt(${limit - val + elVal})`).hide();
//     });
    
//     const totalWrap = $('#reservation-aside-total');
//     const totalBtnEl = $('#reservation-aside-btn');
//     const totalRoomEl = $('#reservation-aside-rooms');
//     const totalPriceEl = $('#reservation-aside-price');
//     let totalPrice = 0;
//     let totalRoom = 0;

//     $('.reservation-options').each(function(index, el){
//         const room = $(el).find('.reservation-options-rooms .f-control').val();
//         const price = $(el).find('.reservation-options-price .new-price-num').text();
//         totalRoom += parseInt(room);
//         totalPrice += parseInt(price * room);
//     });
//     totalRoomEl.text(totalRoom);
//     totalPriceEl.text(totalPrice);
    
//     if(totalRoom > 0){
//         totalBtnEl.attr('disabled', false);
//         totalWrap.attr('hidden', false);
//     }
//     else{
//         totalBtnEl.attr('disabled', true);
//         totalWrap.attr('hidden', true);
//     }
// }

function counterDate(date1, date2){
    const oneDay = 24 * 60 * 60 * 1000;
    const firstDate = new Date(date1);
    const secondDate = new Date(date2);
    return Math.round(Math.abs((firstDate - secondDate) / oneDay));
}

function selectRoom(){
    const $ = jQuery.noConflict();
    const totalWrap = $('#reservation-aside-total');
    const totalBtnEl = $('#reservation-aside-btn');
    const totalRoomEl = $('#reservation-aside-rooms');
    const totalPriceEl = $('#reservation-aside-price');
    const totalDateEl = $('#reservation-aside-date');
    const fromDate = $('#from-date').val();
    const ToDate = $('#to-date').val();
    const couterDate = counterDate(fromDate, ToDate) || 1;
    let totalPrice = 0;
    let totalRoom = 0;
    $('.reservation-options').each(function(index, el){
        const room = $(el).find('.reservation-options-rooms .f-control').val();
        const price = $(el).find('.reservation-options-price .new-price-num').attr('data-price');
        totalRoom += parseInt(room);
        totalPrice += parseInt(price * room);
    });
    totalRoomEl.text(totalRoom);
    let $total = parseInt(totalPrice) * parseInt(couterDate);
    $total = format2($total);
    totalPriceEl.text($total);
    totalDateEl.text(couterDate);
    if(totalRoom > 0){
        totalBtnEl.attr('disabled', false);
        totalWrap.attr('hidden', false);
    }
    else{
        totalBtnEl.attr('disabled', true);
        totalWrap.attr('hidden', true);
    }
}
function format2( n ) {
    a = n.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    return a.replace(/(\.(\d))/ , '');
}
 